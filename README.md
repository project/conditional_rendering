# Conditional Content Block Rendering

This module provides a solution for conditional rendering of content blocks used
on Layout Builder. It allows you to set rules for each block, determining when
they should be rendered based on the specific conditions.

## Features

The basic functionality of this module is to provide a user-friendly interface
for defining conditions under which certain content blocks will be rendered when
used on layout builder.
This can be used to create dynamic, context-sensitive layouts.
For example, you might use this module to show a specific block only to
logged-in users, or to display a block for user that meet a criteria.

## Post-Installation

After installing the module, when you create a new content block or edit an
existing one, you'll find a new section for defining rendering conditions.
You can add as many conditions as you need for each block, and the block will
only be shown or hidden based on the conditions you define.

## Additional Requirements

This module requires Drupal core. No additional modules, libraries, or APIs are
required.

## Recommended Modules

While not required, the [Token](https://www.drupal.org/project/token) module can
be used in conjunction with this module to show available tokens and provide
additional flexibility in defining rendering conditions.
