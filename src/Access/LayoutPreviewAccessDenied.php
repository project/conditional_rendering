<?php

namespace Drupal\conditional_rendering\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Accessible class to allow access for inline blocks in the Layout Builder.
 *
 * @internal
 *   Tagged services are internal.
 */
class LayoutPreviewAccessDenied implements AccessibleInterface {

  /**
   * {@inheritdoc}
   */
  public function access($operation, ?AccountInterface $account = NULL, $return_as_object = FALSE) {

    return AccessResult::forbidden();
  }

}
