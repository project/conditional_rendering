<?php

namespace Drupal\conditional_rendering\EventSubscriber;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\block_content\Access\RefinableDependentAccessInterface;
use Drupal\conditional_rendering\Access\LayoutPreviewAccessDenied;
use Drupal\conditional_rendering\TokenConditionalEvaluationService;
use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Drupal\layout_builder\LayoutBuilderEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Builds render arrays and handles access for all block components.
 *
 * @internal
 *   Tagged services are internal.
 */
class RenderBlockSubscriber implements EventSubscriberInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $currentUser;

  /**
   * Token Evaluation Service.
   *
   * @var \Drupal\conditional_rendering\TokenConditionalEvaluationService
   */
  private $tokenEvaluator;

  /**
   * The Block Content Storage.
   *
   * @var \Drupal\Core\Entity\RevisionableStorageInterface
   */
  private $blockContentStorage;

  /**
   * Creates a BlockComponentRenderArray object.
   *
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\conditional_rendering\TokenConditionalEvaluationService $tokenEvaluator
   *   The Token Evaluator Service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity Type Manager.
   */
  public function __construct(AccountInterface $currentUser, TokenConditionalEvaluationService $tokenEvaluator, EntityTypeManagerInterface $entityTypeManager) {
    $this->currentUser = $currentUser;
    $this->tokenEvaluator = $tokenEvaluator;
    $this->blockContentStorage = $entityTypeManager->getStorage('block_content');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY] = [
      'onBuildRender',
      9999,
    ];
    return $events;
  }

  /**
   * Avoid rendering blocks with active conditional rendering.
   *
   * @param \Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent $event
   *   The section component render event.
   */
  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event) {
    $block = $event->getPlugin();
    $blockIsSetToEvaluateAConditionalRendering = FALSE;

    // Safe early exit.
    if (!$block instanceof BlockPluginInterface) {
      return;
    }

    // Set block access dependency.
    if ($block instanceof RefinableDependentAccessInterface) {
      $contexts = $event->getContexts();

      if (isset($contexts['layout_builder.entity'])) {
        if ($entity = $contexts['layout_builder.entity']->getContextValue()) {
          $blockConfig = $block->getConfiguration();
          if (isset($blockConfig['block_revision_id'])) {

            $block_revision_id = $blockConfig['block_revision_id'];
            $blockEntity = $this->blockContentStorage->loadRevision($block_revision_id);

            if ($blockEntity) {

              $blockIsSetToEvaluateAConditionalRendering = $blockEntity->conditional_rendering_action->value ?? FALSE;

              // Conditional feature must be evaluated just on default LB View.
              if (!$event->inPreview()) {
                if ($blockIsSetToEvaluateAConditionalRendering) {
                  $action = $blockEntity->conditional_rendering_action->value;
                  $conditions = $blockEntity->conditional_rendering_conditions->getValue();

                  $assert = TRUE;
                  foreach ($conditions as $condition) {
                    $assert = $assert && $this->tokenEvaluator->eval(
                      $condition['property'],
                      $condition['operator'],
                      $condition['value']
                    );
                  }

                  switch ($action) {
                    case 'show':
                      $assert ? $block->setAccessDependency($entity) : $block->addAccessDependency(new LayoutPreviewAccessDenied());
                      break;

                    case 'hide':
                      $assert ? $block->addAccessDependency(new LayoutPreviewAccessDenied()) : $block->setAccessDependency($entity);
                      break;

                    default:
                      $block->setAccessDependency($entity);
                      break;
                  }
                }
              }
            }
          }
        }
      }

      // Only check access if the component is not being previewed.
      if ($blockIsSetToEvaluateAConditionalRendering) {
        if ($event->inPreview()) {
          AccessResult::allowed()->setCacheMaxAge(0);
        }
        else {
          $block->access($this->currentUser, TRUE);
        }
      }
    }
  }

}
