<?php

declare(strict_types=1);

namespace Drupal\conditional_rendering\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'conditional_rendering_condition' field type.
 *
 * @FieldType(
 *   id = "conditional_rendering_condition",
 *   label = @Translation("Conditional Rendering Condition"),
 *   category = @Translation("Conditional Rendering"),
 *   list_class = "\Drupal\Core\Field\FieldItemList",
 *   default_widget = "conditional_rendering_condition",
 *   default_formatter = "string",
 * )
 */
final class ConditionItem extends FieldItemBase {
  /**
   * List of available condition types.
   *
   * @var array
   */
  public static $conditions = [
    'equals' => 'Equals',
    'not_equals' => 'Not Equals',
    'contains' => 'Contains',
    'not_contains' => 'Not Contains',
    'starts_with' => 'Starts With',
    'not_starts_with' => 'Not Starts With',
    'ends_with' => 'Ends With',
    'not_ends_with' => 'Not Ends With',
    'empty' => 'Empty',
    'not_empty' => 'Not Empty',
    'greater_than' => 'Greater Than',
    'greater_than_or_equal_to' => 'Greater Than Or Equal To',
    'less_than' => 'Less Than',
    'less_than_or_equal_to' => 'Less Than Or Equal To',
    'in' => 'Is In',
    'not_in' => 'Is Not In',
    'one_of' => 'One Of',
    'not_one_of' => 'Not One Of',
    'regex' => 'Regular Expression',
  ];

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    return match ($this->get('property')->getValue()) {
      NULL, '' => TRUE,
      default => FALSE,
    };
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {

    $properties['operator'] = DataDefinition::create('string')
      ->setLabel(t('Operator'))
      ->setRequired(TRUE);

    $properties['property'] = DataDefinition::create('string')
      ->setLabel(t('Property'))
      ->setRequired(TRUE);

    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Value'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints(): array {
    $constraints = parent::getConstraints();

    $constraint_manager = $this->getTypedDataManager()->getValidationConstraintManager();
    $options = [];

    // Condition must be a valid condition.
    // See /core/lib/Drupal/Core/Validation/Plugin/Validation/Constraint
    // directory for available constraints.
    $constraints[] = $constraint_manager->create('ComplexData', $options);
    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {

    $columns = [
      'operator' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'Operator.',
        'length' => 255,
      ],
      'property' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'Condition Property.',
        'length' => 255,
      ],
      'value' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'Condition Value.',
        'length' => 255,
      ],
    ];

    $schema = [
      'columns' => $columns,
      'indexes' => [],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition): array {
    $random = new Random();
    $values['operator'] = array_rand(self::$conditions);
    $values['property'] = $random->word(mt_rand(1, 50));
    $values['value'] = $random->word(mt_rand(1, 50));
    return $values;
  }

}
