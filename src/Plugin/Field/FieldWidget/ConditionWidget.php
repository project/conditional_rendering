<?php

declare(strict_types=1);

namespace Drupal\conditional_rendering\Plugin\Field\FieldWidget;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\conditional_rendering\Plugin\Field\FieldType\ConditionItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the 'conditional_rendering_condition' field widget.
 *
 * @FieldWidget(
 *   id = "conditional_rendering_condition",
 *   label = @Translation("Conditional Rendering Condition"),
 *   field_types = {"conditional_rendering_condition"},
 * )
 */
final class ConditionWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The Module Handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ModuleHandlerInterface $moduleHandler) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {

    $element['property'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Property'),
      '#default_value' => $items[$delta]->property ?? NULL,
      '#required' => $element['#required'],
      '#size' => 24,
    ];

    if ($this->moduleHandler->moduleExists('token')) {
      $element['tokens'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => [],
      ];
    }

    $element['operator'] = [
      '#type' => 'select',
      '#title' => $this->t('Operator'),
      '#options' => ConditionItem::$conditions,
      '#default_value' => $items[$delta]->operator ?? NULL,
      '#required' => $element['#required'],
    ];

    $element['value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#default_value' => $items[$delta]->value ?? NULL,
      '#required' => $element['#required'],
      '#size' => 24,
    ];

    return $element;
  }

}
