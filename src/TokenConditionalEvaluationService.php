<?php

namespace Drupal\conditional_rendering;

use Drupal\Core\Utility\Token;

/**
 * Class TokenConditionalEvaluationService.
 *
 * Evaluates Conditional Rendering setup against Tokens.
 */
class TokenConditionalEvaluationService {

  /**
   * The Token Service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  private $token;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Utility\Token $token
   *   The Token Service.
   */
  public function __construct(Token $token) {
    $this->token = $token;
  }

  /**
   * Method Eval. Compares two strings.
   *
   * @param string $property
   *   The property to compare.
   * @param string $operator
   *   The desired operation.
   * @param string $value
   *   The value to compare against.
   *
   * @return bool
   *   Returns the result of the comparison.
   */
  public function eval($property, $operator, $value) : bool {
    $eval = FALSE;

    if ($property && $operator) {
      $value = (isset($value)) ? $value : '';

      $tokenProperty = $this->token->replace($property);

      switch ($operator) {
        case 'equals':
          $eval = $tokenProperty == $value;
          break;

        case 'not_equals':
          $eval = $tokenProperty != $value;
          break;

        case 'greater_than':
          $eval = $tokenProperty > $value;
          break;

        case 'greater_than_or_equal_to':
          $eval = $tokenProperty >= $value;
          break;

        case 'less_than':
          $eval = $tokenProperty < $value;
          break;

        case 'less_than_or_equal_to':
          $eval = $tokenProperty <= $value;
          break;

        case 'empty':
          $eval = (strlen($tokenProperty) === 0);
          break;

        case 'not_empty':
          $eval = (strlen($tokenProperty) > 0);
          break;

        case 'contains':
          $eval = strstr($tokenProperty, $value);
          break;

        case 'not_contains':
          $eval = !strstr($tokenProperty, $value);
          break;

        case 'starts_with':
          $eval = (substr($tokenProperty, 0, strlen($value)) === $value);
          break;

        case 'ends_with':
          $eval = (substr($tokenProperty, -strlen($value)) === $value);
          break;

        case 'in':
          $options = explode(',', $tokenProperty);
          $options = array_map('trim', $options);
          $eval = in_array($value, $options);
          break;

        case 'not_in':
          $options = explode(',', $tokenProperty);
          $options = array_map('trim', $options);
          $eval = !in_array($value, $options);
          break;

        case 'one_of':
          $options = explode(',', $value);
          $options = array_map('trim', $options);
          $eval = in_array($tokenProperty, $options);
          break;

        case 'not_one_of':
          $options = explode(',', $value);
          $options = array_map('trim', $options);
          $eval = !in_array($tokenProperty, $options);
          break;

        case 'regex':
          $eval = preg_match($value, $tokenProperty);
          break;

        default:
          $eval = FALSE;
          break;

      }
    }
    return $eval;
  }

}
